package principal.modelos;

public class Bike {
	
	private String marca;
	private int qtdeMarchas;
	private int velocidade;
	private int marchaAtual;
	
	private Quadro quadro;
	private Roda rodaDianteira;
	private Roda rodaTraseira;
	
	//Construtores	
	public Bike() {
	}
	
	public Bike(String marca, int qtdeMarchas, int velocidade, int marchaAtual,
			Quadro quadro, Roda rodaDianteira, Roda rodaTraseira) {
		this.marca = marca;
		this.qtdeMarchas = qtdeMarchas;
		this.velocidade = velocidade;
		this.marchaAtual = marchaAtual;
		this.quadro = quadro;
		this.rodaDianteira = rodaDianteira;
		this.rodaTraseira = rodaTraseira;
	}
	
	
	
	
	//Getters (pegar)
	public String getMarca() {
		return this.marca;
	}
	
	public int getQtdeMarchas() {
		return this.qtdeMarchas;
	}
	
	public int getVelocidade() {
		return this.velocidade;
	}
	
	public int getMarcha() {
		return this.marchaAtual;
	}
		
	
	//Setters (definir)
	public void setMarca(String marca) {
		this.marca = marca;
	}
	
	public void setQtdeMarchas(int qtde) {
		if (qtde > 0 && qtde < 50) {
			this.qtdeMarchas = qtde;
		} else {
			this.qtdeMarchas = 1;
		}	
	}
	
	public void setVelocidade(int velocidade) {
		if (velocidade > 0) {
			this.velocidade = velocidade;
		}
	}
	
	public void setMarcha(int marcha) {
		if (marcha > 0) {
			this.marchaAtual = marcha;
		}
	}

	public Quadro getQuadro() {
		return quadro;
	}

	public void setQuadro(Quadro quadro) {
		this.quadro = quadro;
	}

	public Roda getRodaDianteira() {
		return rodaDianteira;
	}

	public void setRodaDianteira(Roda rodaDianteira) {
		this.rodaDianteira = rodaDianteira;
	}

	public Roda getRodaTraseira() {
		return rodaTraseira;
	}

	public void setRodaTraseira(Roda rodaTraseira) {
		this.rodaTraseira = rodaTraseira;
	}

}
