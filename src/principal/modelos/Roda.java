package principal.modelos;

public class Roda {
	
	private String material;
	private String aro;
	private String cor;
	
	public Roda() {
	}
	
	public Roda(String material, String aro, String cor) {
		this.material = material;		
		this.aro = aro;
		this.cor = cor;
	}

	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getAro() {
		return aro;
	}
	public void setAro(String aro) {
		this.aro = aro;
	}
}
