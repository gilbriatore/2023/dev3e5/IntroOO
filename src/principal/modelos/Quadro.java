package principal.modelos;

public class Quadro {
	
	private String material;
	private String tamanho;
	private String cor;
	
	public Quadro() {
	}
	
	public Quadro(String material, String tamanho, String cor) {
		this.material = material;
		this.tamanho = tamanho;
		this.cor = cor;
	}

	public String getMaterial() {
		return material;
	}
	public void setMaterial(String material) {
		this.material = material;
	}
	public String getTamanho() {
		return tamanho;
	}
	public void setTamanho(String tamanho) {
		this.tamanho = tamanho;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
}
