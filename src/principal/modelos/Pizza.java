package principal.modelos;

import java.util.List;
import java.util.ArrayList;

public class Pizza {

	private String nome;
	private List<Ingrediente> ingredientes = new ArrayList<>();
	
	public void adicionar(Ingrediente ingrediente) {
		//this.ingredientes[0] = ingrediente;
		this.ingredientes.add(ingrediente);
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
	
	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
