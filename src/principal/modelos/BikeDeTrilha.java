package principal.modelos;

public class BikeDeTrilha extends Bike {
	
	
	public BikeDeTrilha(String marca, int qtdeMarchas, int velocidade, int marchaAtual,
			Quadro quadro, Roda rodaDianteira, Roda rodaTraseira) {
		
		super(marca, qtdeMarchas, velocidade, marchaAtual,
				quadro, rodaDianteira, rodaTraseira);
		
	}

}
