package principal.modelos;

public class Pessoa {

	private String nome;
	private String CPF;
	private double peso;

	//Método construtor tem sempre o 
	//nome exato da classe.
	public Pessoa() {		
	}
	
	//Método construtor não tem retorno,
	//pq retorna sempre um objeto do tipo da classe. 
	public Pessoa(String nome) {
		this.nome = nome;
	}	
	
	public Pessoa(String nome, String CPF) {
		this.nome = nome;
		this.CPF = CPF;
	}
	
	public Pessoa(String nome, String CPF, double peso) {		
		this.nome = nome;
		this.CPF = CPF;
		this.peso = peso;
	}

	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getCPF() {
		return CPF;
	}
	public void setCPF(String cPF) {
		CPF = cPF;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}

}
