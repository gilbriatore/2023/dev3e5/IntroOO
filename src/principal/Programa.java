package principal;

import java.util.List;

import principal.modelos.*;

public class Programa {
	
	public static void main(String[] args) {
		
		Ingrediente queijo = new Ingrediente("Queijo", "Parmesão");
		Ingrediente molho = new Ingrediente("Molho", "Tomate");
		
		Pizza minhaPizza = new Pizza();
		minhaPizza.setNome("Queijo e tomate");
		minhaPizza.adicionar(queijo);
		minhaPizza.adicionar(molho);
		
		System.out.println("Pizza: " + minhaPizza.getNome());
		
		List<Ingrediente> lista = minhaPizza.getIngredientes();
		for(int i = 0; i < lista.size(); i++) {
			Ingrediente ingrediente = lista.get(i);
			System.out.println("Ingrediente: " + ingrediente.getNome());
		}
				
//		for(Ingrediente ingrediente : minhaPizza.getIngredientes()) {
//			System.out.println("Ingrediente: " + ingrediente.getNome());
//		}
		
		 
	}
	
//	public static void main(String[] args) {
//		
////		BikeDeTrilha bikeDeTrilha = new BikeDeTrilha();
////		bikeDeTrilha.setQuadro(new Quadro());
//		
//		
//		//CRIAÇÃO DA BIKE
//		BikeDeTrilha minhaBike = new BikeDeTrilha("Caloi", 10, 10, 2, 
//				new Quadro("Alumínio", "Médio", "Vermelha"), 
//				new Roda("Alumínio", "18", "Alumínio"), 
//				new Roda("Alumínio", "18", "Branca"));
//		
//		//Pintura da cor do quadro
//		minhaBike.getQuadro().setCor("Amarela");
//			
//		
//		//IMPRESSÃO DOS DADOS DA BIKE
//		System.out.println("Marca: " + minhaBike.getMarca());
//		System.out.println("Qtde marchas: " + minhaBike.getQtdeMarchas());
//		System.out.println("Marcha: " + minhaBike.getMarcha());
//		System.out.println("Velocidade: " + minhaBike.getVelocidade());
//				
//		System.out.println("Cor do quadro: " + minhaBike.getQuadro().getCor()); 
//		System.out.println("Material do quadro: " + minhaBike.getQuadro().getMaterial());
//		System.out.println("Tamanho do quadro: " + minhaBike.getQuadro().getTamanho());
//
//		System.out.println("Cor da rodaDianteira: " + minhaBike.getRodaDianteira().getCor()); 
//		System.out.println("Material da rodaDianteira: " + minhaBike.getRodaDianteira().getMaterial());
//		System.out.println("Aro da rodaDianteira: " + minhaBike.getRodaDianteira().getAro());
//		
//		System.out.println("Cor da rodaTraseira: " + minhaBike.getRodaTraseira().getCor()); 
//		System.out.println("Material da rodaTraseira: " + minhaBike.getRodaTraseira().getMaterial());
//		System.out.println("Aro da rodaTraseira: " + minhaBike.getRodaTraseira().getAro());
//		
//	}
	
	
//   public static void main(String[] args) {
//		
//		//CRIAÇÃO DA BIKE
//		Bike minhaBike = new Bike();
//		minhaBike.setMarca("Caloi");
//		minhaBike.setQtdeMarchas(10);
//		minhaBike.setMarcha(2);
//		minhaBike.setVelocidade(10);
//		
//		Quadro quadroDaBike = new Quadro();
//		quadroDaBike.setMaterial("Alumínio");
//		quadroDaBike.setTamanho("Médio");
//		quadroDaBike.setCor("Vermelha");
//		
//		minhaBike.setQuadro(quadroDaBike); 
//		
//		Roda rodaDianteira = new Roda();
//		rodaDianteira.setMaterial("Alumínio");
//		rodaDianteira.setAro("18");
//		rodaDianteira.setCor("Alúminio");
//		
//		minhaBike.setRodaDianteira(rodaDianteira);
//		
//		Roda rodaTraseira = new Roda();
//		rodaTraseira.setMaterial("Alumínio");
//		rodaTraseira.setAro("18");
//		rodaTraseira.setCor("Alumínio");
//		
//		minhaBike.setRodaTraseira(rodaTraseira);		
//		
//		//IMPRESSÃO DOS DADOS DA BIKE
//		System.out.println("Marca: " + minhaBike.getMarca());
//		System.out.println("Qtde marchas: " + minhaBike.getQtdeMarchas());
//		System.out.println("Marcha: " + minhaBike.getMarcha());
//		System.out.println("Velocidade: " + minhaBike.getVelocidade());
//				
//		System.out.println("Cor do quadro: " + minhaBike.getQuadro().getCor()); 
//		System.out.println("Material do quadro: " + minhaBike.getQuadro().getMaterial());
//		System.out.println("Tamanho do quadro: " + minhaBike.getQuadro().getTamanho());
//
//		System.out.println("Cor da rodaDianteira: " + minhaBike.getRodaDianteira().getCor()); 
//		System.out.println("Material da rodaDianteira: " + minhaBike.getRodaDianteira().getMaterial());
//		System.out.println("Aro da rodaDianteira: " + minhaBike.getRodaDianteira().getAro());
//		
//		System.out.println("Cor da rodaTraseira: " + minhaBike.getRodaTraseira().getCor()); 
//		System.out.println("Material da rodaTraseira: " + minhaBike.getRodaTraseira().getMaterial());
//		System.out.println("Aro da rodaTraseira: " + minhaBike.getRodaTraseira().getAro());
//		
//	}
}








